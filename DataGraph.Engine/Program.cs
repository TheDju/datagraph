﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataGraph.Common;
using DataGraph.Caching;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Diagnostics;

namespace DataGraph.Engine
{
    class Program
    {
        struct SampleData
        {
            public DateTime Date;
            public string Key;
            public object Value;
        }

        struct FreezeData
        {
            public DateTime Time;
            public string User;
            public string Station;
            public int Duration;
            public string Version;
            public string Type;
        }

        static void Main(string[] args)
        {
            //TestIoT();
            TestWatchdog();

            //Console.ReadKey(true);
        }

        private static void TestIoT()
        {
            DateTime start = DateTime.Now;

            // Build a small sample data collection
            Random random = new Random(24);
            IEnumerable<SampleData> data = Enumerable.Range(0, 86400)
                                                     .SelectMany(i =>
                                                     {
                                                         DateTime date = new DateTime(2016, 1, 1, 0, 0, 0).AddSeconds(i);

                                                         return new SampleData[]
                                                         {
                                                             new SampleData() { Date = date, Key = "Temperature", Value = (float)random.NextDouble() * 5 + 18 },
                                                             new SampleData() { Date = date, Key = "Luminosity", Value = (float)random.NextDouble() * 40 + 40 }
                                                         };
                                                     });

            // Perform some data manipulation
            var collection = ObjectTable.Build(data).Cached();
            var result1 = collection.Where(v => (string)v["Key"] == "Temperature")
                                    .GroupBy(nameof(SampleData.Date), Projections.Dates.Round(TimeSpan.FromHours(2)));
            var result2 = collection.Where(v => (string)v["Key"] == "Luminosity")
                                    .GroupBy(nameof(SampleData.Date), Projections.Dates.Round(TimeSpan.FromHours(2)));
            
            // Show result
            foreach (var value in result1)
                Console.WriteLine(string.Join(", ", value.Values.Select(v => v.Key.Name + ": " + (v.Value ?? "null"))));
            foreach (var value in result2)
                Console.WriteLine(string.Join(", ", value.Values.Select(v => v.Key.Name + ": " + (v.Value ?? "null"))));

            Console.WriteLine();
            Console.WriteLine("Results generated in " + (int)(DateTime.Now - start).TotalMilliseconds + " ms");
        }
        private static void TestWatchdog()
        {
            DateTime start = DateTime.Now;

            List<FreezeData> data = new List<FreezeData>();

            // Load data from CSV
            foreach (string line in File.ReadLines(@"..\..\..\Samples\Watchdog - 2016-09-30.csv").Skip(1))
            {
                string[] fields = line.Split(';').Select(f => f.Trim('"')).ToArray();

                FreezeData freeze = new FreezeData()
                {
                    Time = DateTime.Parse(fields[0]),
                    User = fields[1],
                    Station = fields[2],
                    Duration = int.Parse(fields[3]),
                    Version = fields[4],
                    Type = fields[5],
                };

                data.Add(freeze);
            }

            // Perform some data manipulation
            DataTable rawData = ObjectTable.Build(data)
                .Where(v => ((DateTime)v["Time"]).Hour >= 8 && ((DateTime)v["Time"]).AddMinutes(-1).Hour <= 21)
                .Cached();

            //ShowTable(rawData, 5);

            // Collect fields
            Field timeField = rawData["Time"],
                  durationField = rawData["Duration"];

            // Build virtual fields
            VirtualField siteField = new VirtualField("Site", v => v["Station"].ToString().Substring(0, 3)),
                         roundTimeField = new VirtualField("Time", v => Projections.Dates.Round(((DateTime)v["Time"]).TimeOfDay, TimeSpan.FromMinutes(10)));

            // Build a pivot table
            PivotTable pivotTable = new PivotTable(rawData)
            {
                RowFields = new[] { roundTimeField },
                ColumnFields = new[] { siteField },
                Value = Aggregations.Aggregate(Aggregations.Sum, roundTimeField)
            };

            ShowTable(pivotTable);

            Console.ReadLine();

            // Build a graph
            Graph graph = new Graph()
            {
                HorizontalField = timeField,
                HorizontalCaptions = new UnitCaptioning()
                {
                    Format = "HH:mm",
                    MainUnit = TimeSpan.FromMinutes(60)
                },
                Margin = new Graph.Thickness(32),
                Elements = new Element[]
                {
                    new PointCloud(rawData, durationField, siteField)
                    {
                        Captions = new CountCaptioning(10) { Position = CaptionPosition.Left }
                    },
                    new Curve(rawData.GroupBy(nameof(FreezeData.Time), Projections.Dates.Round(TimeSpan.FromMinutes(15)), Aggregations.Average), durationField, Color.Black),
                }
            };

            // Render to bitmap
            using (Bitmap bitmap = new Bitmap(1280, 720))
            {
                graph.Render(bitmap);
                bitmap.Save("Output.png");
            }

            // Ugly way to show picture in Windows 10
            Process.Start("Output.png");

            Console.WriteLine("Count: " + rawData.Count());
        }
        
        private static void ShowTable(DataTable table, int limit = int.MaxValue)
        {
            Console.WriteLine(string.Join(", ", table.Fields));

            foreach (var value in table.Take(limit))
                Console.WriteLine(string.Join(", ", value.Values.Select(v => v.Value ?? "null")));

            Console.WriteLine();
        }
        private static void ShowCollection(IEnumerable<Value> collection, int limit = int.MaxValue)
        {
            foreach (var value in collection.Take(limit))
                Console.WriteLine(string.Join(", ", value.Values.Select(v => v.Value ?? "null")));

            Console.WriteLine();
        }
    }
}