﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataGraph.Caching;
using DataGraph.Common;

namespace DataGraph.Engine
{
    public class Curve : Element
    {
        public override IEnumerable<Value> Values { get; }
        public Field Field { get; }

        public ValueChooser<Pen> LinePen { get; }
        public ValueChooser<Marker> Marker { get; set; }

        public object MinValue { get; set; }
        public object MaxValue { get; set; }

        public Curve(IEnumerable<Value> values, Field field) : this(values, field, Color.Black)
        {
        }
        public Curve(IEnumerable<Value> values, Field field, Color color)
        {
            Values = values.Cached();
            Field = field;

            LinePen = v => new Pen(color);
            Marker = v => new Marker(color);
        }
        public Curve(IEnumerable<Value> values, Field field, Field colorField)
        {
            Values = values.Cached();
            Field = field;

            LinePen = ValueDefaultPenChooser(v => v[colorField]);
            Marker = ValueDefaultMarkerChooser(v => v[colorField]);
        }

        public override void Draw(Graph.DrawContext context)
        {
            Value[] values = Values.OrderBy(context.Graph.HorizontalField).ToArray();

            object minValue = MinValue ?? values.Min(v => v[Field]);
            object maxValue = MaxValue ?? values.Max(v => v[Field]);

            float? lastX = null,
                   lastY = null;

            foreach (Value value in values)
            {
                float valueX = Helpers.Map(value[context.Graph.HorizontalField], context.MinHorizontalValue, context.MaxHorizontalValue, context.GraphX, context.GraphX + context.GraphWidth);
                float valueY = Helpers.Map(value[Field], minValue, maxValue, context.GraphY, context.GraphY - context.GraphHeight);

                Pen linePen = LinePen(value);
                Marker marker = Marker(value);

                if (lastX != null)
                    context.Graphics.DrawLine(linePen, lastX.Value, lastY.Value, valueX, valueY);

                context.Graphics.DrawMarker(marker, valueX, valueY);

                lastX = valueX;
                lastY = valueY;
            }
        }
    }
}