﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataGraph.Common;

namespace DataGraph.Engine
{
    public class Curve
    {
        public Field Field { get; }

        public Pen LinePen { get; }
        public Brush MarkerBrush { get; }

        public Curve(Field field, Color color)
        {
            Field = field;

            LinePen = new Pen(color);
            MarkerBrush = new SolidBrush(color);
        }
    }
}