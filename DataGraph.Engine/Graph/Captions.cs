﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataGraph.Common;

namespace DataGraph.Engine
{
    public enum CaptionPosition
    {
        Left,
        Top,
        Right,
        Bottom
    }

    public struct Caption
    {
        public object Value { get; }
        public float Position { get; }
        public bool Main { get; }

        public Caption(object value, float position, bool main = true)
        {
            Value = value;
            Position = position;
            Main = main;
        }
    }

    public abstract class Captioning
    {
        public CaptionPosition Position { get; set; }

        public string Format { get; set; }

        public abstract IEnumerable<Caption> GetCaptions(object minValue, object maxValue);
    }

    public class SparseCaptioning : Captioning
    {
        public override IEnumerable<Caption> GetCaptions(object minValue, object maxValue)
        {
            throw new NotImplementedException();
        }
    }

    public class UnitCaptioning : Captioning
    {
        public object MainUnit { get; set; }
        public object SecondaryUnit { get; set; }

        public override IEnumerable<Caption> GetCaptions(object minValue, object maxValue)
        {
            FieldType type = FieldType.GetType(minValue);

            if (type == FieldType.Number)
            {
                decimal value = FieldType.ToDecimal(minValue);
                decimal limit = FieldType.ToDecimal(maxValue);
                decimal step = FieldType.ToDecimal(MainUnit);
                float stepPosition = (float)(step / (limit - value));

                float position = 0;

                while (value < limit)
                {
                    yield return new Caption(value, position);

                    value += step;
                    position += stepPosition;
                }
            }
            else if (type == FieldType.Date)
            {
                long value = FieldType.ToTicks(minValue);
                long limit = FieldType.ToTicks(maxValue);
                long step = FieldType.ToTicks(MainUnit);
                float stepPosition = (float)((decimal)step / (limit - value));

                float position = 0;

                while (value < limit)
                {
                    yield return new Caption(new DateTime(value), position);

                    value += step;
                    position += stepPosition;
                }
            }
            else
                throw new NotSupportedException("Count captioning is not supported for this object type");
        }
    }
    public class CountCaptioning : Captioning
    {
        public uint Count { get; set; }

        public CountCaptioning(uint count)
        {
            Count = count;
        }
        public CountCaptioning(uint count, string format)
        {
            Count = count;
            Format = format;
        }

        public override IEnumerable<Caption> GetCaptions(object minValue, object maxValue)
        {
            if (Count == 0)
                yield break;

            yield return new Caption(minValue, 0);

            FieldType type = FieldType.GetType(minValue);

            if (type == FieldType.Number)
            {
                decimal value = FieldType.ToDecimal(minValue);
                decimal diff = FieldType.ToDecimal(maxValue) - value;

                for (int i = 0; i < Count - 1; i++)
                {
                    value += diff / (Count - 1);
                    yield return new Caption(value, 1.0f / (Count - 1) * (i + 1));
                }
            }
            else if (type == FieldType.Date)
            {
                long value = FieldType.ToTicks(minValue);
                long diff = FieldType.ToTicks(maxValue) - value;

                for (int i = 0; i < Count - 1; i++)
                {
                    value += diff / (Count - 1);
                    yield return new Caption(new DateTime(value), 1.0f / (Count - 1) * (i + 1));
                }
            }
            else
                throw new NotSupportedException("Count captioning is not supported for this object type");
        }
    }
}