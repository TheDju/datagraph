﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataGraph.Caching;
using DataGraph.Common;

namespace DataGraph.Engine
{
    public class PointCloud : Element
    {
        public override IEnumerable<Value> Values { get; }
        public Field Field { get; }

        public ValueChooser<Marker> Marker { get; set; }

        public PointCloud(IEnumerable<Value> values, Field field) : this(values, field, Color.Black) { }
        public PointCloud(IEnumerable<Value> values, Field field, Color color)
        {
            Values = values.Cached();
            Field = field;

            Marker = v => new Marker(color);
        }
        public PointCloud(IEnumerable<Value> values, Field field, Field colorField)
        {
            Values = values.Cached();
            Field = field;

            Marker = ValueDefaultMarkerChooser(v => v[colorField]);
        }

        public override void Draw(Graph.DrawContext context)
        {
            Value[] values = Values.OrderBy(context.Graph.HorizontalField).ToArray();

            object minValue = MinValue ?? values.Min(v => v[Field]);
            object maxValue = MaxValue ?? values.Max(v => v[Field]);

            // Draw the graphics
            foreach (Value value in values)
            {
                float valueX = Helpers.Map(value[context.Graph.HorizontalField], context.MinHorizontalValue, context.MaxHorizontalValue, context.GraphX, context.GraphX + context.GraphWidth);
                float valueY = Helpers.Map(value[Field], minValue, maxValue, context.GraphY, context.GraphY - context.GraphHeight);

                Marker marker = Marker(value);
                context.Graphics.DrawMarker(marker, valueX, valueY);
            }

            // Draw the captions

        }
        public override IEnumerable<Caption> GetCaptions()
        {
            /*Dictionary<string, Color> captions = new Dictionary<string, Color>();

            foreach (Value value in Values)
            {
                Marker marker 
            }

            Color[] colors = Values.Select(v => Marker(v))
                                   .Select(m => m.Color)
                                   .Distinct*/

            yield break;
        }
    }
}