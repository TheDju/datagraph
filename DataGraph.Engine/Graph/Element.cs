﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataGraph.Common;

namespace DataGraph.Engine
{
    public delegate T ValueChooser<T>(Value value);

    public abstract class Element
    {
        #region Helpers

        private static Color[] colors = new[] { 0x5b9bd5, 0xed7d31, 0x70ad47, 0xffc000, 0x44546a, 0xa5a5a5 }
            .Select(c => Color.FromArgb(unchecked((int)(0xFF000000 | c))))
            .ToArray();

        private static object nullObject = new object();

        public static ValueChooser<Color> ValueDefaultColorChooser(Func<Value, object> projection)
        {
            Hashtable objectColors = new Hashtable();

            return v =>
            {
                object value = projection(v) ?? nullObject;
                Color color;

                if (objectColors.Contains(value))
                    color = (Color)objectColors[value];
                else
                    objectColors.Add(value, color = colors[objectColors.Count % colors.Length]);

                return color;
            };
        }
        public static ValueChooser<Pen> ValueDefaultPenChooser(Func<Value, object> projection)
        {
            ValueChooser<Color> colorChooser = ValueDefaultColorChooser(projection);
            return v => new Pen(colorChooser(v));
        }
        public static ValueChooser<Brush> ValueDefaultBrushChooser(Func<Value, object> projection)
        {
            ValueChooser<Color> colorChooser = ValueDefaultColorChooser(projection);
            return v => new SolidBrush(colorChooser(v));
        }
        public static ValueChooser<Marker> ValueDefaultMarkerChooser(Func<Value, object> projection, MarkerShape shape = Marker.DefaultShape, float size = Marker.DefaultSize)
        {
            ValueChooser<Color> colorChooser = ValueDefaultColorChooser(projection);
            return v => new Marker(colorChooser(v), size, shape);
        }

        #endregion

        public abstract IEnumerable<Value> Values { get; }

        public Captioning Captions { get; set; }

        public object MinValue { get; set; }
        public object MaxValue { get; set; }

        public abstract void Draw(Graph.DrawContext context);
        public virtual IEnumerable<Caption> GetCaptions()
        {
            yield break;
        }
    }
}