﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataGraph.Common;

namespace DataGraph.Engine
{
    public enum MarkerShape
    {
        Circle,
        Square
    }

    public struct Marker
    {
        public const float DefaultSize = 5.5f;
        public const MarkerShape DefaultShape = MarkerShape.Circle;

        public Color Color { get; }
        public float Size { get; }
        public MarkerShape Shape { get; }

        public Marker(Color color) : this(color, DefaultSize, DefaultShape) { }
        public Marker(Color color, float size) : this(color, DefaultSize, DefaultShape) { }
        public Marker(Color color, float size, MarkerShape shape)
        {
            Shape = shape;
            Color = color;
            Size = size;
        }
    }

    public static class GraphicsExtensions
    {
        public static void DrawMarker(this Graphics me, Marker marker, float x, float y)
        {
            Brush brush = new SolidBrush(marker.Color);

            switch (marker.Shape)
            {
                case MarkerShape.Circle:
                {
                    me.FillEllipse(brush, x - marker.Size / 2, y - marker.Size / 2, marker.Size, marker.Size);
                    break;
                }
                case MarkerShape.Square:
                {
                    me.FillRectangle(brush, x - marker.Size / 2, y - marker.Size / 2, marker.Size, marker.Size);
                    break;
                }
            }
        }
    }
}