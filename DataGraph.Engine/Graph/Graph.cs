﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataGraph.Common;

namespace DataGraph.Engine
{
    public class Graph
    {
        public struct Thickness
        {
            public float Left { get; }
            public float Top { get; }
            public float Right { get; }
            public float Bottom { get; }

            public Thickness(float value)
            {
                Left = Top = Right = Bottom = value;
            }
            public Thickness(float left, float top, float right, float bottom)
            {
                Left = left;
                Top = top;
                Right = right;
                Bottom = bottom;
            }
        }
        public class DrawContext
        {
            public Graph Graph { get; }

            public Graphics Graphics { get; set; }
            public int SurfaceWidth { get; set; }
            public int SurfaceHeight { get; set; }

            public float GraphX { get; set; }
            public float GraphY { get; set; }
            public float GraphWidth { get; set; }
            public float GraphHeight { get; set; }

            public object MinHorizontalValue { get; set; }
            public object MaxHorizontalValue { get; set; }

            public DrawContext(Graph graph)
            {
                Graph = graph;
            }
        }

        // Horizontal axis configuration
        public Field HorizontalField { get; set; }
        public Captioning HorizontalCaptions { get; set; }
        public bool HorizontalSparseValues { get; set; } = false;

        public bool ShowVerticalUnits { get; set; } = true;
        public Color VerticalMainUnitColor { get; set; } = Color.LightGray;

        public Color Background { get; set; } = Color.White;
        public Color Foreground { get; set; } = Color.Black;
        public Font Font { get; set; }

        public Thickness Margin { get; set; } = new Thickness(0);

        public Element[] Elements { get; set; }
        
        public void Render(Bitmap bitmap)
        {
            Value[] values = Elements.SelectMany(e => e.Values).OrderBy(HorizontalField).ToArray();

            // Graph style
            Font axisFont = Font ?? SystemFonts.DefaultFont;
            Pen axisPen = new Pen(Foreground);
            Brush axisTextBrush = new SolidBrush(Foreground);

            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                graphics.SmoothingMode = SmoothingMode.AntiAlias;

                DrawContext context = new DrawContext(this)
                {
                    Graphics = graphics,
                    SurfaceWidth = bitmap.Width,
                    SurfaceHeight = bitmap.Height,

                    GraphX = Margin.Left,
                    GraphY = bitmap.Height - 1 - Margin.Top,
                    GraphWidth = bitmap.Width - Margin.Left - Margin.Right,
                    GraphHeight = bitmap.Height - Margin.Top - Margin.Bottom,

                    MinHorizontalValue = values.First()[HorizontalField],
                    MaxHorizontalValue = values.Last()[HorizontalField]
                };

                OnDrawBackground(context);
                OnDrawAxis(context);
                OnDrawCaptions(context);

                // Elements
                foreach (Element element in Elements)
                {
                    element.Draw(context);
                }
            }
        }

        protected virtual void OnDrawBackground(DrawContext context)
        {
            context.Graphics.Clear(Background);
        }
        protected virtual void OnDrawAxis(DrawContext context)
        {
            float axisBorderLength = 6;

            Font axisFont = SystemFonts.DefaultFont;
            Pen axisPen = new Pen(Foreground);
            Brush axisTextBrush = new SolidBrush(Foreground);

            context.Graphics.DrawLine(axisPen, context.GraphX - 1 - axisBorderLength, context.GraphY + 1, context.GraphX - 1 + context.GraphWidth, context.GraphY + 1);
            context.Graphics.DrawLine(axisPen, context.GraphX - 1, context.GraphY + 1 + axisBorderLength, context.GraphX - 1, context.GraphY + 1 - context.GraphHeight);

            if (HorizontalSparseValues)
            {

            }
            else
            {
                //string minHorizontalText = string.IsNullOrEmpty(HorizontalAxisFormat) ? context.MinHorizontalValue.ToString() : string.Format("{0:" + HorizontalAxisFormat + "}", context.MinHorizontalValue);
                //SizeF minHorizontalSize = context.Graphics.MeasureString(minHorizontalText, axisFont);
                //context.Graphics.DrawString(minHorizontalText, axisFont, axisTextBrush, context.GraphX - minHorizontalSize.Width / 2, context.GraphY + axisBorderLength + 2);
            }
        }
        protected virtual void OnDrawCaptions(DrawContext context)
        {
            Captioning captioning = HorizontalCaptions ?? new UnitCaptioning();

            Pen axisPen = new Pen(VerticalMainUnitColor);
            Font axisFont = Font ?? SystemFonts.DefaultFont;
            Brush axisBrush = new SolidBrush(Color.Black);
            float axisBorderLength = 8;

            foreach (Caption caption in captioning.GetCaptions(context.MinHorizontalValue, context.MaxHorizontalValue))
            {
                int x = (int)(context.GraphX + caption.Position * context.GraphWidth);

                if (axisBorderLength > 0)
                    context.Graphics.DrawLine(axisPen, x, context.GraphY, x, context.GraphY + axisBorderLength);

                if (ShowVerticalUnits && caption.Position > 0)
                    context.Graphics.DrawLine(axisPen, x, context.GraphY - context.GraphHeight, x, context.GraphY);

                string text = string.IsNullOrEmpty(captioning.Format) ? caption.Value.ToString() : string.Format("{0:" + captioning.Format + "}", caption.Value);
                SizeF textSize = context.Graphics.MeasureString(text, axisFont);
                context.Graphics.DrawString(text, axisFont, axisBrush, x - textSize.Width / 2, context.GraphY + axisBorderLength + 2);
            }
        }
    }
}