﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataGraph.Common;

namespace DataGraph.MySQL
{
    public class MySqlDataTable : DataTable
    {
        public string Host { get; set; }
        public ushort Port { get; set; }
        public string Database { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public override IEnumerable<Field> Fields
        {
            get
            {
                if (innerDataTable == null)
                    Connect();

                return innerDataTable.Fields;
            }
        }

        private AdoNetDataTable innerDataTable;

        private void Connect()
        {
            MySqlConnection connection = new MySqlConnection("XXX");
            connection.Open();

            innerDataTable = connection;
        }

        public override IEnumerator<Value> GetEnumerator()
        {
            if (innerDataTable == null)
                Connect();

            return innerDataTable.GetEnumerator();
        }
    }
}
