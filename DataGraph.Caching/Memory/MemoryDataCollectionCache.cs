﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataGraph.Common;

namespace DataGraph.Caching.Memory
{
    internal static class MemoryDataCollectionCache
    {
        private static Dictionary<string, MemoryCachedDataCollection> cachedDataCollections = new Dictionary<string, MemoryCachedDataCollection>();
        private static Dictionary<string, MemoryCachedDataTable> cachedDataTables = new Dictionary<string, MemoryCachedDataTable>();

        public static MemoryCachedDataCollection Get(string identifier, IEnumerable<Value> collection)
        {
            MemoryCachedDataCollection cachedCollection;

            if (!cachedDataCollections.TryGetValue(identifier, out cachedCollection))
                cachedDataCollections.Add(identifier, cachedCollection = new MemoryCachedDataCollection(identifier, collection));

            return cachedCollection;
        }
        public static MemoryCachedDataTable Get(string identifier, DataTable table)
        {
            MemoryCachedDataTable cachedTable;

            if (!cachedDataTables.TryGetValue(identifier, out cachedTable))
                cachedDataTables.Add(identifier, cachedTable = new MemoryCachedDataTable(identifier, table));

            return cachedTable;
        }
    }
}