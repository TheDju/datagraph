﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataGraph.Common;

namespace DataGraph.Caching.Memory
{
    internal class MemoryCachedValue : Value
    {
        public MemoryCachedValue(Value other) : base(other.Values.ToDictionary(p => p.Key, p => p.Value)) { }
    }

    internal class MemoryCachedDataCollection : CachedDataCollection
    {
        private IEnumerable<Value> source;
        private MemoryCachedValue[] cache;

        public MemoryCachedDataCollection(string identifier, IEnumerable<Value> collection) : base(identifier)
        {
            source = collection;

            Save();
        }

        public override void Save()
        {
            cache = source.Select(v => new MemoryCachedValue(v)).ToArray();
            values = cache;

            Date = DateTime.Now;
        }
    }
    internal class MemoryCachedDataTable : CachedDataTable
    {
        public override IEnumerable<Field> Fields
        {
            get
            {
                return fields;
            }
        }

        private DataTable source;
        private Field[] fields;
        private MemoryCachedValue[] cache;

        public MemoryCachedDataTable(string identifier, DataTable table) : base(identifier)
        {
            source = table;

            Save();
        }

        public override void Save()
        {
            cache = source.Select(v => new MemoryCachedValue(v)).ToArray();
            fields = source.Fields.ToArray();
            values = cache;

            Date = DateTime.Now;
        }
    }
}