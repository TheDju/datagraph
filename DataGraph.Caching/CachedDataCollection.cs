﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataGraph.Common;

namespace DataGraph.Caching
{
    public abstract class CachedDataCollection : DataCollection
    {
        public virtual string Identifier { get; protected set; }
        public virtual DateTime? Date { get; protected set; }

        public CachedDataCollection(string identifier)
        {
            Identifier = identifier;
        }

        public virtual bool Load() => false;
        public abstract void Save();
    }

    public abstract class CachedDataTable : DataTable
    {
        public virtual string Identifier { get; protected set; }
        public virtual DateTime? Date { get; protected set; }

        public CachedDataTable(string identifier)
        {
            Identifier = identifier;
        }

        public virtual bool Load() => false;
        public abstract void Save();
    }
}