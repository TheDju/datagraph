﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataGraph.Common;

namespace DataGraph.Caching.Memory
{
    internal class FileCachedDataCollection : CachedDataCollection
    {
        private IEnumerable<Value> source;
        private MemoryCachedValue[] cache;

        public FileCachedDataCollection(string identifier, IEnumerable<Value> collection) : base(identifier)
        {
            source = collection;

            Save();
        }

        public override void Save()
        {
            cache = source.Select(v => new MemoryCachedValue(v)).ToArray();
            values = cache;

            Date = DateTime.Now;
        }
    }
    internal class FileCachedDataTable : CachedDataTable
    {
        public override IEnumerable<Field> Fields
        {
            get
            {
                return fields;
            }
        }
        public string FileName => $"{Identifier}.cache.csv";

        private DataTable source;
        private Field[] fields;
        private Value[] cache;

        public FileCachedDataTable(string identifier, DataTable table) : base(identifier)
        {
            source = table;

            if (!Load())
                Save();
        }

        public override bool Load()
        {
            FileInfo fileInfo = new FileInfo(FileName);

            if (!fileInfo.Exists)
                return false;

            Date = fileInfo.LastWriteTime;

            using (StreamReader reader = new StreamReader(fileInfo.FullName))
            {
                string header = reader.ReadLine();
                string[] headerParts = header.Split(';');

                fields = headerParts.Select(p => new Field(p)).ToArray();

                List<Value> dataValues = new List<Value>();

                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] lineValues = line.Split(';');

                    Dictionary<Field, object> values = new Dictionary<Field, object>();
                    for (int i = 0; i < fields.Length; i++)
                        values.Add(fields[i], lineValues[i]);

                    Value value = new Value(values);
                    dataValues.Add(value);
                }

                values = cache = dataValues.ToArray();
            }

            return true;
        }
        public override void Save()
        {
            fields = source.Fields.ToArray();
            values = cache = source.Select(v => new MemoryCachedValue(v)).ToArray();

            using (StreamWriter writer = new StreamWriter(FileName))
            {
                writer.WriteLine(string.Join(";", fields.Select(f => f.Name)));

                foreach (Value value in cache)
                    writer.WriteLine(string.Join(";", value.Values.Select(v => v.Value)));
            }

            FileInfo fileInfo = new FileInfo(FileName);
            Date = fileInfo.LastWriteTime;
        }
    }
}