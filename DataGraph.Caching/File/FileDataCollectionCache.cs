﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataGraph.Common;

namespace DataGraph.Caching.Memory
{
    internal static class FileDataCollectionCache
    {
        private static Dictionary<string, FileCachedDataCollection> cachedDataCollections = new Dictionary<string, FileCachedDataCollection>();
        private static Dictionary<string, FileCachedDataTable> cachedDataTables = new Dictionary<string, FileCachedDataTable>();

        public static FileCachedDataCollection Get(string identifier, IEnumerable<Value> collection)
        {
            FileCachedDataCollection cachedCollection;

            if (!cachedDataCollections.TryGetValue(identifier, out cachedCollection))
                cachedDataCollections.Add(identifier, cachedCollection = new FileCachedDataCollection(identifier, collection));

            return cachedCollection;
        }
        public static FileCachedDataTable Get(string identifier, DataTable table)
        {
            FileCachedDataTable cachedTable;

            if (!cachedDataTables.TryGetValue(identifier, out cachedTable))
                cachedDataTables.Add(identifier, cachedTable = new FileCachedDataTable(identifier, table));

            return cachedTable;
        }
    }
}