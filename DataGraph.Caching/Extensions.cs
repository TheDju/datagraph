﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataGraph.Caching.Memory;
using DataGraph.Common;

namespace DataGraph.Caching
{
    public static class DataCollectionExtensions
    {
        public static CachedDataCollection Cached(this IEnumerable<Value> me)
        {
            string identifier = me.GetType().FullName + ":0x" + me.GetHashCode().ToString("x");
            return me.Cached(identifier);
        }
        public static CachedDataCollection Cached(this IEnumerable<Value> me, string identifier)
        {
            return me.Cached(identifier, false);
        }
        public static CachedDataCollection Cached(this IEnumerable<Value> me, string identifier, bool refresh)
        {
            CachedDataCollection collection = DataCollectionCache.Get(identifier, me);

            if (refresh)
                collection.Save();

            return collection;
        }
        public static CachedDataCollection Cached(this IEnumerable<Value> me, string identifier, DataCollectionCacheMode mode)
        {
            return me.Cached(identifier, mode, false);
        }
        public static CachedDataCollection Cached(this IEnumerable<Value> me, string identifier, DataCollectionCacheMode mode, bool refresh)
        {
            CachedDataCollection collection = null;

            switch (mode)
            {
                case DataCollectionCacheMode.Memory: collection = MemoryDataCollectionCache.Get(identifier, me); break;
                case DataCollectionCacheMode.File: collection = FileDataCollectionCache.Get(identifier, me); break;
                default:
                    throw new NotSupportedException();
            }

            if (refresh)
                collection.Save();

            return collection;
        }

        public static CachedDataTable Cached(this DataTable me)
        {
            string identifier = me.GetType().FullName + ":0x" + me.GetHashCode().ToString("x");
            return me.Cached(identifier);
        }
        public static CachedDataTable Cached(this DataTable me, string identifier)
        {
            return me.Cached(identifier, false);
        }
        public static CachedDataTable Cached(this DataTable me, string identifier, bool refresh)
        {
            CachedDataTable collection = DataCollectionCache.Get(identifier, me);

            if (refresh)
                collection.Save();

            return collection;
        }
        public static CachedDataTable Cached(this DataTable me, string identifier, DataCollectionCacheMode mode)
        {
            return me.Cached(identifier, mode, false);
        }
        public static CachedDataTable Cached(this DataTable me, string identifier, DataCollectionCacheMode mode, bool refresh)
        {
            CachedDataTable table = null;

            switch (mode)
            {
                case DataCollectionCacheMode.Memory: table = MemoryDataCollectionCache.Get(identifier, me); break;
                case DataCollectionCacheMode.File: table = FileDataCollectionCache.Get(identifier, me); break;
                default:
                    throw new NotSupportedException();
            }

            if (refresh)
                table.Save();

            return table;
        }
    }
}