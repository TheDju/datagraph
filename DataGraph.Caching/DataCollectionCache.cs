﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataGraph.Caching.Memory;
using DataGraph.Common;

namespace DataGraph.Caching
{
    public enum DataCollectionCacheMode
    {
        Memory,
        File,
        Database
    }

    public static class DataCollectionCache
    {
        public static DataCollectionCacheMode Mode
        {
            get
            {
                return mode;
            }
            set
            {
                if (mode == value)
                    return;

                mode = value;
                cachedDataCollections.Clear();
            }
        }

        private static DataCollectionCacheMode mode = DataCollectionCacheMode.Memory;
        private static Dictionary<string, CachedDataCollection> cachedDataCollections = new Dictionary<string, CachedDataCollection>();
        private static Dictionary<string, CachedDataTable> cachedDataTables = new Dictionary<string, CachedDataTable>();

        public static CachedDataCollection Get(string identifier, IEnumerable<Value> collection)
        {
            CachedDataCollection cachedCollection;

            if (!cachedDataCollections.TryGetValue(identifier, out cachedCollection))
            {
                switch (mode)
                {
                    case DataCollectionCacheMode.Memory: cachedCollection = MemoryDataCollectionCache.Get(identifier, collection); break;
                    case DataCollectionCacheMode.File: cachedCollection = FileDataCollectionCache.Get(identifier, collection); break;
                }

                if (collection == null)
                    throw new Exception("Unable to find a cached data collection matching identifier " + identifier);

                cachedDataCollections.Add(identifier, cachedCollection);
            }

            return cachedCollection;
        }
        public static CachedDataTable Get(string identifier, DataTable table)
        {
            CachedDataTable cachedTable;

            if (!cachedDataTables.TryGetValue(identifier, out cachedTable))
            {
                switch (mode)
                {
                    case DataCollectionCacheMode.Memory: cachedTable = MemoryDataCollectionCache.Get(identifier, table); break;
                    //case DataCollectionCacheMode.File: cachedTable = FileDataCollectionCache.Get(identifier, table); break;
                }

                if (table == null)
                    throw new Exception("Unable to find a cached data collection matching identifier " + identifier);

                cachedDataTables.Add(identifier, cachedTable);
            }

            return cachedTable;
        }
    }
}