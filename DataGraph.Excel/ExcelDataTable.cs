﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataGraph.Common;

using OfficeOpenXml;
using OfficeOpenXml.Table;

namespace DataGraph.Excel
{
    public class ExcelDataTable : DataTable
    {
        public FileInfo File { get; }
        public string Sheet { get; set; }
        public string Table { get; set; }

        public override IEnumerable<Field> Fields
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        private ExcelPackage package;
        private ExcelWorksheet worksheet;
        private ExcelTable table;

        public ExcelDataTable(FileInfo file, string sheet)
        {
            File = file;
            Sheet = sheet;
        }
        public ExcelDataTable(FileInfo file, string sheet, string table)
        {
            File = file;
            Sheet = sheet;
            Table = table;
        }

        private void Load()
        {
            package = new ExcelPackage(File);

            worksheet = package.Workbook.Worksheets.FirstOrDefault(s => s.Name == Sheet);
            if (worksheet == null)
                throw new Exception("Could not find a worksheet with the specified name");

            if (string.IsNullOrEmpty(Table))
                table = worksheet.Tables.FirstOrDefault();
            else
                table = worksheet.Tables.FirstOrDefault(t => t.Name == Sheet);

            if (table == null)
                throw new Exception("Could not find a table with the specified name");
        }

        public override IEnumerator<Value> GetEnumerator()
        {


            yield break;
        }
    }
}