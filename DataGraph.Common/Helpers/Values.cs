﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public static class Helpers
    {
        public static int Compare(object left, object right)
        {
            FieldType type = FieldType.GetType(left);

            if (type == FieldType.Number)
            {
                decimal leftValue = Convert.ToDecimal(left);
                decimal rightValue = Convert.ToDecimal(right);

                return leftValue.CompareTo(rightValue);
            }
            else if (type == FieldType.Date)
            {
                DateTime leftValue = (DateTime)left;
                DateTime rightValue = (DateTime)right;

                return leftValue.CompareTo(rightValue);
            }

            throw new NotSupportedException();
        }

        public static decimal GetRatio(object value, object sourceFrom, object sourceTo)
        {
            FieldType sourceType = FieldType.GetType(sourceFrom);

            if (sourceType == FieldType.Number)
            {
                decimal from = Convert.ToDecimal(sourceFrom);
                decimal ratio = (Convert.ToDecimal(value) - from) / (Convert.ToDecimal(sourceTo) - from);

                return ratio;
            }
            else if (sourceType == FieldType.Date)
            {
                DateTime from = (DateTime)sourceFrom;
                decimal ratio = (decimal)((DateTime)value - from).Ticks / ((DateTime)sourceTo - from).Ticks;

                return ratio;
            }
            
            throw new NotSupportedException();
        }

        public static object Map(object value, object sourceFrom, object sourceTo, object destinationFrom, object destinationTo)
        {
            decimal ratio = GetRatio(value, sourceFrom, sourceTo);
            return Map(ratio, destinationFrom, destinationTo);
        }
        public static object Map(decimal ratio, object destinationFrom, object destinationTo)
        {
            FieldType destinationType = FieldType.GetType(destinationFrom);

            if (destinationType == FieldType.Number)
            {
                decimal to = Convert.ToDecimal(destinationFrom);
                return to + ratio * (Convert.ToDecimal(destinationTo) - to);
            }
            else if (destinationType == FieldType.Date)
            {
                DateTime to = (DateTime)destinationFrom;
                return new DateTime(to.Ticks + (long)(ratio * ((DateTime)destinationTo - to).Ticks));
            }

            throw new NotSupportedException();
        }

        public static float Map(object value, object sourceFrom, object sourceTo, float destinationFrom, float destinationTo)
        {
            decimal ratio = GetRatio(value, sourceFrom, sourceTo);
            return Map(ratio, destinationFrom, destinationTo);
        }
        public static float Map(decimal ratio, float destinationFrom, float destinationTo)
        {
            return destinationFrom + (float)ratio * (destinationTo - destinationFrom);
        }

        public static object Add(object left, object right)
        {
            FieldType leftType = FieldType.GetType(left);
            FieldType rightType = FieldType.GetType(right);

            if (leftType != rightType)
                throw new NotSupportedException("Both objects to add must have the same type");

            if (leftType == FieldType.Number)
                return Convert.ToDecimal(left) + Convert.ToDecimal(right);
            else if (leftType == FieldType.Date)
            {
                long ticks = FieldType.ToTicks(left) + FieldType.ToTicks(right);
                return new DateTime(ticks);
            }
            else if (leftType == FieldType.Text)
                return (string)left + (string)right;
            
            throw new NotSupportedException("Could not determine objects type");
        }
    }
}