﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public static class Ordering
    {
        public static IEnumerable<Value> OrderBy(this IEnumerable<Value> me, string field)
        {
            return me.OrderBy(v => v[field]);
        }
        public static IEnumerable<Value> OrderBy(this IEnumerable<Value> me, Field field)
        {
            return me.OrderBy(v => v[field]);
        }
        public static IEnumerable<Value> OrderByDescending(this IEnumerable<Value> me, string field)
        {
            return me.OrderByDescending(v => v[field]);
        }
        public static IEnumerable<Value> OrderByDescending(this IEnumerable<Value> me, Field field)
        {
            return me.OrderByDescending(v => v[field]);
        }
    }
}