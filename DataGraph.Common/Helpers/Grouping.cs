﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public delegate object Projection(object value);
    public delegate object FieldAggregation(IEnumerable<object> values);
    public delegate Value Aggregation(IEnumerable<Value> values);
    public delegate object ValueFieldAggregation(IEnumerable<Value> values);

    public static class Projections
    {
        public static object Identity(object value)
        {
            return value;
        }

        public static class Numbers
        {
            public static object Ceiling(object value)
            {
                if (value == null)
                    return null;

                if (value is float)
                    return (float)Math.Ceiling((float)value);
                else if (value is double)
                    return Math.Ceiling((double)value);
                else if (value is byte || value is sbyte || value is short || value is ushort || value is int || value is uint || value is long || value is ulong)
                    return value;

                throw new NotSupportedException("Could not compute ceiling of a value of type " + value.GetType());
            }
            public static object Floor(object value)
            {
                if (value == null)
                    return null;

                if (value is float)
                    return (float)Math.Floor((float)value);
                else if (value is double)
                    return Math.Floor((double)value);
                else if (value is byte || value is sbyte || value is short || value is ushort || value is int || value is uint || value is long || value is ulong)
                    return value;

                throw new NotSupportedException("Could not compute ceiling of a value of type " + value.GetType());
            }
        }
        public static class Dates
        {
            public static Projection Round(TimeSpan span)
            {
                return v => Round(v, span);
            }
            public static object Round(object value, TimeSpan span)
            {
                if (value == null)
                    return null;

                if (value is DateTime)
                {
                    long ticks = ((DateTime)value).Ticks;
                    return new DateTime(ticks - (ticks % span.Ticks));
                }
                else if (value is TimeSpan)
                {
                    long ticks = ((TimeSpan)value).Ticks;
                    return new TimeSpan(ticks - (ticks % span.Ticks));
                }

                throw new NotSupportedException("Could not compute round of a value of type " + value.GetType());
            }
        }
    }

    public static class Aggregations
    {
        public static object Sum(IEnumerable<object> values)
        {
            FieldType type = null;

            string text = null;
            decimal sum = 0;

            foreach (object value in values)
            {
                if (value == null)
                    continue;

                FieldType valueType = FieldType.GetType(value);
                if (type != null && type != valueType)
                    return null;
                if (type == null)
                    type = valueType;

                if (type == FieldType.Number)
                    sum += FieldType.ToDecimal(value);
                else if (type == FieldType.Date)
                    sum += FieldType.ToTicks(value);
                else if (type == FieldType.Text)
                {
                    if (text == null)
                        text = (string)value;
                    else if (text != (string)value)
                        return null;
                }
                else
                    return null;
            }

            if (type == FieldType.Number)
                return sum;
            else if (type == FieldType.Date)
                return new DateTime((long)sum);
            else if (type == FieldType.Text)
                return text;

            return null;
        }
        public static object Average(IEnumerable<object> values)
        {
            FieldType type = null;

            string text = null;
            decimal sum = 0;
            int count = 0;

            foreach (object value in values)
            {
                if (value == null)
                    continue;

                FieldType valueType = FieldType.GetType(value);
                if (type != null && type != valueType)
                    return null;
                if (type == null)
                    type = valueType;

                if (type == FieldType.Number)
                {
                    sum += Convert.ToDecimal(value);
                    count++;
                }
                else if (type == FieldType.Date)
                {
                    sum += ((DateTime)value).Ticks;
                    count++;
                }
                else if (type == FieldType.Text)
                {
                    if (text == null)
                        text = (string)value;
                    else if (text != (string)value)
                        return null;
                }
                else
                    return null;
            }

            if (type == FieldType.Number)
                return sum / count;
            else if (type == FieldType.Date)
                return new DateTime((long)(sum / count));
            else if (type == FieldType.Text)
                return text;

            return null;
        }
        public static object Min(IEnumerable<object> values)
        {
            FieldType type = null;
            object result = null;

            foreach (object value in values)
            {
                if (value == null)
                    continue;

                FieldType valueType = FieldType.GetType(value);
                if (type != null && type != valueType)
                    return null;
                if (type == null)
                    type = valueType;

                if (result == null)
                    result = value;
                else
                {
                    if (type == FieldType.Text)
                    {
                        if (string.Compare(FieldType.ToString(value), FieldType.ToString(result)) < 0)
                            result = value;
                    }
                    else if (type == FieldType.Number)
                    {
                        if (FieldType.ToDecimal(value) < FieldType.ToDecimal(result))
                            result = value;
                    }
                    else if (type == FieldType.Date)
                    {
                        if (FieldType.ToTicks(value) < FieldType.ToTicks(result))
                            result = value;
                    }
                    else
                        return null;
                }
            }

            return result;
        }
        public static object Max(IEnumerable<object> values)
        {
            FieldType type = null;
            object result = null;

            foreach (object value in values)
            {
                if (value == null)
                    continue;

                FieldType valueType = FieldType.GetType(value);
                if (type != null && type != valueType)
                    return null;
                if (type == null)
                    type = valueType;

                if (result == null)
                    result = value;
                else
                {
                    if (type == FieldType.Text)
                    {
                        if (string.Compare(FieldType.ToString(value), FieldType.ToString(result)) > 0)
                            result = value;
                    }
                    else if (type == FieldType.Number)
                    {
                        if (FieldType.ToDecimal(value) > FieldType.ToDecimal(result))
                            result = value;
                    }
                    else if (type == FieldType.Date)
                    {
                        if (FieldType.ToTicks(value) > FieldType.ToTicks(result))
                            result = value;
                    }
                    else
                        return null;
                }
            }
            
            return result;
        }
        public static object First(IEnumerable<object> values)
        {
            return values.First();
        }
        public static object Last(IEnumerable<object> values)
        {
            return values.Last();
        }

        public static Value Sum(IEnumerable<Value> values)
        {
            return new AggregatedValue(values, Sum);
        }
        public static Value Average(IEnumerable<Value> values)
        {
            return new AggregatedValue(values, Average);
        }
        public static Value Min(IEnumerable<Value> values)
        {
            return new AggregatedValue(values, Min);
        }
        public static Value Max(IEnumerable<Value> values)
        {
            return new AggregatedValue(values, Max);
        }
        public static Value First(IEnumerable<Value> values)
        {
            return new AggregatedValue(values, First);
        }
        public static Value Last(IEnumerable<Value> values)
        {
            return new AggregatedValue(values, Last);
        }

        public static ValueFieldAggregation Aggregate(FieldAggregation aggregation, string field)
        {
            return values => aggregation(values.Select(v => v[field]));
        }
        public static ValueFieldAggregation Aggregate(FieldAggregation aggregation, Field field)
        {
            return values => aggregation(values.Select(v => field.GetObject(v)));
        }

        public static Value Concat(IEnumerable<Value> values)
        {
            return null;
        }
        public static Value Concat(IEnumerable<Value> values, string separator)
        {
            return null;
        }
        public static Aggregation Concat(string separator)
        {
            return v => Concat(v, separator);
        }
    }
}