﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public static class FilteringOperation
    {
        public static IEnumerable<Value> GroupBy(this IEnumerable<Value> me, string field)
        {
            return me.GroupBy(field, Projections.Identity, Aggregations.First);
        }
        public static IEnumerable<Value> GroupBy(this IEnumerable<Value> me, string field, Projection projection)
        {
            return me.GroupBy(field, projection, Aggregations.First);
        }
        public static IEnumerable<Value> GroupBy(this IEnumerable<Value> me, string field, Aggregation aggregation)
        {
            return me.GroupBy(field, Projections.Identity, aggregation);
        }
        public static IEnumerable<Value> GroupBy(this IEnumerable<Value> me, string field, Projection projection, Aggregation aggregation)
        {
            projection = projection ?? Projections.Identity;
            aggregation = aggregation ?? Aggregations.Last;

            return me.GroupBy(v => projection(v[field])).Select(g => new ProjectedValue(aggregation(g), field, g.Key));
        }

        public static IEnumerable<Value> GroupBy(this IEnumerable<Value> me, Field field)
        {
            return me.GroupBy(field, Projections.Identity, Aggregations.First);
        }
        public static IEnumerable<Value> GroupBy(this IEnumerable<Value> me, Field field, Projection projection)
        {
            return me.GroupBy(field, projection, Aggregations.First);
        }
        public static IEnumerable<Value> GroupBy(this IEnumerable<Value> me, Field field, Aggregation aggregation)
        {
            return me.GroupBy(field, Projections.Identity, aggregation);
        }
        public static IEnumerable<Value> GroupBy(this IEnumerable<Value> me, Field field, Projection projection, Aggregation aggregation)
        {
            projection = projection ?? Projections.Identity;
            aggregation = aggregation ?? Aggregations.Last;

            return me.GroupBy(v => projection(v[field])).Select(g => new ProjectedValue(aggregation(g), field, g.Key));
        }
    }
}