﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public static class GroupingOperation
    {
        public static DataTable Where(this DataTable me, Func<Value, bool> predicate)
        {
            return new DataTable(me.Fields, Enumerable.Where(me, predicate));
        }
    }
}