﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public class FieldType
    {
        public static FieldType Text { get; } = new FieldType("Text");
        public static FieldType Number { get; } = new FieldType("Number");
        public static FieldType Date { get; } = new FieldType("Date");

        public string Name { get; }

        private FieldType(string name)
        {
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }

        public static FieldType GetType(object value)
        {
            return GetType(value?.GetType());
        }
        public static FieldType GetType(Type type)
        {
            if (type == null)
                throw new NullReferenceException();

            if (type == typeof(string) || type == typeof(char))
                return Text;
            if (type == typeof(float) || type == typeof(double) || type == typeof(byte) || type == typeof(sbyte) || type == typeof(short) || type == typeof(ushort) || type == typeof(int) || type == typeof(uint) || type == typeof(long) || type == typeof(ulong) || type == typeof(decimal))
                return Number;
            if (type == typeof(DateTime) || type == typeof(TimeSpan))
                return Date;

            return null;
        }

        public static decimal ToDecimal(object value)
        {
            return Convert.ToDecimal(value);
        }
        public static long ToTicks(object value)
        {
            if (value is TimeSpan)
                return ((TimeSpan)value).Ticks;
            else if (value is DateTime)
                return ((DateTime)value).Ticks;

            throw new NotSupportedException("The specified value is not a Date");
        }
        public static string ToString(object value)
        {
            if (value is string)
                return (string)value;
            else if (value is char)
                return ((char)value).ToString();

            throw new NotSupportedException("The specified value is not a Text");
        }
    }

    public class Field
    {
        public string Name { get; set; }
        public FieldType Type { get; set; }

        public Field(string name)
        {
            Name = name;
        }
        public Field(string name, FieldType type)
        {
            Name = name;
            Type = type;
        }

        public virtual object GetObject(Value value)
        {
            return value[this];
        }
        public virtual decimal GetNumber(Value value)
        {
            return FieldType.ToDecimal(GetObject(value));
        }
        public virtual long GetTicks(Value value)
        {
            return FieldType.ToTicks(GetObject(value));
        }
        public virtual string GetString(Value value)
        {
            return GetObject(value) as string;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public static class FieldExtensions
    {
        private static Dictionary<Type, List<Field>> typeFields = new Dictionary<Type, List<Field>>();

        public static IEnumerable<Field> GetDataFields(this Type me)
        {
            List<Field> fields;

            if (!typeFields.TryGetValue(me, out fields))
            {
                fields = new List<Field>();

                foreach (PropertyInfo property in me.GetProperties())
                    fields.Add(new ObjectField(property.Name, property));

                foreach (FieldInfo field in me.GetFields())
                    fields.Add(new ObjectField(field.Name, field));

                typeFields.Add(me, fields);
            }

            return fields;
        }
    }
}