﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public delegate object VirtualFieldGetter(Value value);

    public class VirtualField : Field
    {
        private VirtualFieldGetter projection;

        public VirtualField(string name, VirtualFieldGetter projection) : base(name)
        {
            this.projection = projection;
        }
        public VirtualField(string name, FieldType type, VirtualFieldGetter projection) : base(name, type)
        {
            this.projection = projection;
        }

        public override object GetObject(Value value)
        {
            return projection(value);
        }
    }
}