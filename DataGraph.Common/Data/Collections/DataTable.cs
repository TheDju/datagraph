﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public class DataTable : DataCollection
    {
        public virtual IEnumerable<Field> Fields
        {
            get
            {
                return fields;
            }
        }

        public Field this[int index]
        {
            get
            {
                return Fields.ElementAt(index);
            }
        }
        public Field this[string name]
        {
            get
            {
                return Fields.FirstOrDefault(f => f.Name == name);
            }
        }

        private List<Field> fields;

        protected DataTable() { }
        public DataTable(IEnumerable<Field> fields, IEnumerable<Value> enumerable) : base(enumerable)
        {
            this.fields = fields.ToList();
        }
        public DataTable(IEnumerable<Field> fields, IEnumerable enumerable) : base(enumerable)
        {
            this.fields = fields.ToList();
        }
        
        public static DataTable LoadFile(string path, char separator = ';', char encloser = '"', Encoding encoding = null, bool readHeader = true)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException("Could not find specified file", path);

            encoding = encoding ?? Encoding.Default;

            IEnumerable<string> lines = File.ReadLines(path, encoding);
            IEnumerator<string> linesEnumerator = lines.GetEnumerator();

            if (!linesEnumerator.MoveNext())
                return null;

            string[] headers;

            // Decode headers
            if (readHeader)
            {
                headers = linesEnumerator.Current.Split(separator);
                linesEnumerator.MoveNext();
            }
            else
                headers = linesEnumerator.Current.Split(separator).Select((f, i) => i.ToString()).ToArray();

            // Read data

            return null;
        }

        public override IEnumerator<Value> GetEnumerator()
        {
            return values.GetEnumerator();
        }
    }
}