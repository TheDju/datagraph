﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public class ObjectTable : DataTable
    {
        public ObjectTable(Type type, IEnumerable enumerable) : base(type.GetDataFields(), enumerable) { }
        
        public static ObjectTable Build<T>(IEnumerable<T> enumerable)
        {
            return new ObjectTable(typeof(T), enumerable);
        }

        public override IEnumerator<Value> GetEnumerator()
        {
            foreach (object obj in collection)
                yield return new ObjectValue(obj);
        }
    }
}