﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public class AdoNetDataTable : DataTable
    {
        public DbConnection Connection { get; }
        public string Query { get; }

        public override IEnumerable<Field> Fields
        {
            get
            {
                if (dataReader == null)
                    Execute();

                return fields;
            }
        }

        private DbCommand command;
        private DbDataReader dataReader;
        private Field[] fields;

        private AdoNetDataTable() { }
        public AdoNetDataTable(DbConnection connection, string query)
        {
            Connection = connection;
            Query = query;
        }

        private void Execute()
        {
            command = Connection.CreateCommand();
            command.CommandText = Query;

            dataReader = command.ExecuteReader();

            // Cache fields
            fields = new Field[dataReader.FieldCount];

            for (int i = 0; i < dataReader.FieldCount; i++)
            {
                string name = dataReader.GetName(i);
                Type type = dataReader.GetFieldType(i);

                fields[i] = new Field(name, FieldType.GetType(type));
            }
        }

        public override IEnumerator<Value> GetEnumerator()
        {
            if (dataReader == null)
                Execute();

            while (dataReader.Read())
            {
                Dictionary<Field, object> values = new Dictionary<Field, object>();

                for (int i = 0; i < dataReader.FieldCount; i++)
                    values.Add(fields[i], dataReader.GetFieldValue<object>(i));

                yield return new Value(values);
            }
        }
    }
}
