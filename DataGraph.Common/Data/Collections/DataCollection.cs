﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public class DataCollection : IEnumerable<Value>
    {
        protected IEnumerable collection;
        protected IEnumerable<Value> values;

        protected DataCollection() { }
        public DataCollection(IEnumerable enumerable)
        {
            collection = enumerable;
            values = collection.OfType<object>().Select(i => new ObjectValue(i));
        }
        public DataCollection(IEnumerable<Value> enumerable)
        {
            collection = values = enumerable;
        }

        public virtual DataCollection OrderBy(string field)
        {
            return new DataCollection(values.OrderBy(v => v[field]));
        }
        public virtual DataCollection OrderBy(Field field)
        {
            return new DataCollection(values.OrderBy(v => v[field]));
        }
        public virtual DataCollection OrderByDescending(string field)
        {
            return new DataCollection(values.OrderByDescending(v => v[field]));
        }
        public virtual DataCollection OrderByDescending(Field field)
        {
            return new DataCollection(values.OrderByDescending(v => v[field]));
        }

        public virtual IEnumerator<Value> GetEnumerator()
        {
            return values.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}