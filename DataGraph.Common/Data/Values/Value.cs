﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public class Value
    {
        public virtual IEnumerable<Field> Fields
        {
            get
            {
                return values.Keys;
            }
        }
        public virtual IReadOnlyDictionary<Field, object> Values
        {
            get
            {
                return values;
            }
        }

        protected Dictionary<Field, object> values;

        public object this[string key]
        {
            get
            {
                Field field = Values.Keys.Single(f => f.Name == key);
                return Values[field];
            }
        }
        public object this[Field field]
        {
            get
            {
                return Values[field];
            }
        }

        protected Value()
        {
            values = new Dictionary<Field, object>();
        }
        public Value(Dictionary<Field, object> values)
        {
            this.values = values;
        }
    }
}