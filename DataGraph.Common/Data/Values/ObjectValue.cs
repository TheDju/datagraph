﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public class ObjectField : Field
    {
        public MemberInfo Member { get; private set; }

        public ObjectField(string name, MemberInfo member) : base(name)
        {
            Member = member;
        }
    }

    public class ObjectValue : Value
    {
        private static Dictionary<Type, List<Field>> typeFields = new Dictionary<Type, List<Field>>();

        public object Object { get; private set; }
        public Type Type { get; private set; }

        public override IEnumerable<Field> Fields
        {
            get
            {
                if (fields == null)
                    fields = Type.GetDataFields().ToList();

                return fields;
            }
        }
        public override IReadOnlyDictionary<Field, object> Values
        {
            get
            {
                if (values == null)
                {
                    values = Fields.OfType<ObjectField>().ToDictionary(f => (Field)f, f =>
                    {
                        if (f.Member is PropertyInfo)
                            return (f.Member as PropertyInfo).GetValue(Object);
                        else if (f.Member is FieldInfo)
                            return (f.Member as FieldInfo).GetValue(Object);

                        return null;
                    });
                }

                return values;
            }
        }

        private List<Field> fields;
        private Dictionary<Field, object> values;

        public ObjectValue(object obj)
        {
            Object = obj;
            Type = obj.GetType();
        }
    }
}