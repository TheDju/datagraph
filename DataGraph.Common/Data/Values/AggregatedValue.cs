﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public class AggregatedValue : Value
    {
        public IEnumerable<Value> Source { get; private set; }
        public FieldAggregation Aggregation { get; private set; }

        public override IEnumerable<Field> Fields
        {
            get
            {
                return Source.First().Fields;
            }
        }
        public override IReadOnlyDictionary<Field, object> Values
        {
            get
            {
                return Fields.ToDictionary(f => f, f => Aggregation(Source.Select(v => v[f.Name])));
            }
        }

        public AggregatedValue(IEnumerable<Value> source, FieldAggregation aggregation)
        {
            Source = source;
            Aggregation = aggregation;
        }
    }
}