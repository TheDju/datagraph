﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public delegate object VirtualFieldCallback(Value value);

    public class VirtualValue : Value
    {
        public Value Source { get; private set; }
        public Dictionary<Field, VirtualFieldCallback> VirtualFields { get; private set; }

        public override IEnumerable<Field> Fields
        {
            get
            {
                return VirtualFields.Keys;
            }
        }
        public override IReadOnlyDictionary<Field, object> Values
        {
            get
            {
                return VirtualFields.ToDictionary(p => p.Key, p => p.Value(Source));
            }
        }

        public VirtualValue(Value source, Dictionary<Field, VirtualFieldCallback> virtualFields)
        {
            Source = source;
            VirtualFields = virtualFields;
        }
    }
}