﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public class ProjectedValue : Value
    {
        public Value Source { get; private set; }
        public string Field { get; private set; }
        public object Value { get; private set; }

        public override IEnumerable<Field> Fields
        {
            get
            {
                return Source.Fields;
            }
        }
        public override IReadOnlyDictionary<Field, object> Values
        {
            get
            {
                return Source.Values.ToDictionary(p => p.Key, p => p.Key.Name == Field ? Value : p.Value);
            }
        }

        public ProjectedValue(Value source, string field, object value)
        {
            Source = source;
            Field = field;
            Value = value;
        }
        public ProjectedValue(Value source, Field field, object value)
        {
            Source = source;
            Field = field.Name;
            Value = value;
        }
    }
}