﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGraph.Common
{
    public class PivotTable : DataTable
    {
        internal class CachedValue : Value
        {
            public CachedValue(Value other) : base(other.Values.ToDictionary(p => p.Key, p => p.Value)) { }
        }
        internal class PivotGroup
        {
            public object Value { get; }
            public List<PivotGroup> Children { get; } = new List<PivotGroup>();
            public List<Value> Values { get; } = new List<Value>();

            public PivotGroup(object value)
            {
                Value = value;
            }
        }
        internal class PivotField : Field
        {
            public PivotGroup[] Groups { get; }

            public PivotField(IEnumerable<PivotGroup> groups, string name) : base(name)
            {
                Groups = groups as PivotGroup[] ?? groups.ToArray();
            }
        }

        public IEnumerable<Value> Source { get; private set; }

        public IEnumerable<Field> RowFields
        {
            get
            {
                return rowFields;
            }
            set
            {
                rowFields = value as Field[] ?? value?.ToArray();
            }
        }
        public IEnumerable<Field> ColumnFields
        {
            get
            {
                return columnFields;
            }
            set
            {
                columnFields = value as Field[] ?? value?.ToArray();
            }
        }

        public ValueFieldAggregation Value { get; set; }

        public override IEnumerable<Field> Fields
        {
            get
            {
                if (fields == null)
                {
                    Evaluate();

                    fields = new List<PivotField>();
                    BuildFields(fields, new PivotGroup[0], columnGroups);
                }

                foreach (Field field in RowFields)
                    yield return field;
                foreach (Field field in fields)
                    yield return field;
            }
        }

        private List<PivotField> fields = null;
        private new CachedValue[] values;

        private Field[] rowFields = null;
        private List<PivotGroup> rowGroups = null;
        private Field[] columnFields = null;
        private List<PivotGroup> columnGroups = null;

        public PivotTable(IEnumerable<Value> source)
        {
            Source = source;
        }

        private void Evaluate()
        {
            values = Source.Select(v => new CachedValue(v)).ToArray();

            // Evaluate pivot groups
            rowGroups = new List<PivotGroup>();
            EvaluatePivotGroups(rowGroups, rowFields, values);

            if (columnFields != null)
            {
                columnGroups = new List<PivotGroup>();
                EvaluatePivotGroups(columnGroups, columnFields, values);
            }
        }
        private void EvaluatePivotGroups(List<PivotGroup> groups, Field[] fields, IEnumerable<Value> values)
        {
            foreach (Value value in values)
            {
                object[] fieldValues = fields.Select(f => f.GetObject(value)).ToArray(); // FIXME: May throw an exception if f is not part of value

                List<PivotGroup> currentGroups = groups;
                PivotGroup lastGroup = null;

                for (int i = 0; i < fieldValues.Length; i++)
                {
                    object fieldValue = fieldValues[i];

                    PivotGroup matchingGroup = currentGroups.FirstOrDefault(g => (g.Value == null && fieldValue == null) || (g.Value?.Equals(fieldValue) ?? false));
                    if (matchingGroup == null)
                        currentGroups.Add(matchingGroup = new PivotGroup(fieldValue));

                    lastGroup = matchingGroup;
                    currentGroups = matchingGroup.Children;
                }

                lastGroup.Values.Add(value);
            }
        }

        private void BuildFields(List<PivotField> fields, PivotGroup[] parents, IEnumerable<PivotGroup> groups)
        {
            if (groups == null)
            {
                fields.Add(new PivotField(null, "Value"));
            }
            else
            {
                foreach (PivotGroup group in groups)
                {
                    PivotGroup[] chain = parents.Concat(new[] { group }).ToArray();

                    if (group.Children.Count == 0)
                        fields.Add(new PivotField(chain, group.Value?.ToString() ?? "Value"));
                    else
                        BuildFields(fields, chain, group.Children);
                }
            }
        }

        public override IEnumerator<Value> GetEnumerator()
        {
            foreach (PivotGroup group in rowGroups)
                foreach (Value value in EnumerateGroup(group, Enumerable.Empty<PivotGroup>()))
                    yield return value;
        }
        private IEnumerable<Value> EnumerateGroup(PivotGroup group, IEnumerable<PivotGroup> parents)
        {
            PivotGroup[] chain = parents.Concat(new[] { group }).ToArray();

            if (group.Children.Count > 0)
            {
                foreach (PivotGroup g in rowGroups)
                    foreach (Value value in EnumerateGroup(g, chain))
                        yield return value;
            }
            else
            {
                Dictionary<PivotField, List<Value>> groupValues = fields.ToDictionary(f => f, f => new List<Value>());

                foreach (Value value in group.Values)
                {
                    object[] fieldValues = ColumnFields.Select(f => f.GetObject(value)).ToArray(); // FIXME: May throw an exception if f is not part of value

                    PivotField matchingField = null;
                    foreach (PivotField field in fields)
                    {
                        bool matching = true;

                        for (int i = 0; i < field.Groups.Length; i++)
                        {
                            object groupValue = field.Groups[i].Value;
                            matching &= (fieldValues[i] == null && groupValue == null) || (fieldValues[i]?.Equals(groupValue) ?? false);
                        }

                        if (matching)
                        {
                            matchingField = field;
                            break;
                        }
                    }
                    
                    groupValues[matchingField].Add(value);
                }


                Dictionary<Field, object> finalValues = new Dictionary<Field, object>();

                for (int i = 0; i < rowFields.Length; i++)
                    finalValues.Add(rowFields[i], chain[i].Value);
                foreach (var pair in groupValues)
                    finalValues.Add(pair.Key, Value(pair.Value));

                yield return new Value(finalValues);
            }
        }
    }

    public static class PivotTableExtensions
    {
        public static PivotTable Pivot(this DataCollection collection, string column, string row, string value)
        {
            Value[] values = collection.ToArray();




            throw new NotImplementedException();
        }
        public static PivotTable Pivot(this DataCollection collection, Field column, Field row, Field value)
        {
            return Pivot(collection, column.Name, row.Name, value.Name);
        }
    }
}